from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render, get_object_or_404
from blog.models import Blog, Comment
from django.utils import timezone
from time import strftime




def menu(request):
	return render(request, 'blog/menu.html', {'now': strftime('%c')}) 
	
def aboutMe(request):
	return render(request, 'blog/aboutMe.html' ,{'now': strftime('%c')})
	
def gitNotes(request):
	return render(request, 'blog/gitNotes.html' ,{'now': strftime('%c')})

def blogHome(request):
	
	latestBlog = Blog.objects.order_by('-post_date')[:3]
	

	ctx = {'latestBlog' : latestBlog}
	
	return render(request, 'blog/blogHome.html', ctx)
	
def blogArchive(request):
	latestBlogs = Blog.objects.order_by('-post_date')
	ctx = {'latestBlog' : latestBlogs}
	
	return render(request, 'blog/blogHome.html', ctx)

	
	
	
def blogEntry(request, blog_id):
	blogs = Blog.objects.get(pk = blog_id)
	latestComment = blogs.comment_set.order_by('-post_date')
	
	
		
	return render(request, 'blog/blogEntry.html', {'blog': blogs, 'latestComment': latestComment})
	
def comment(request, id):
		blog = get_object_or_404(Blog, pk=id)
		try:
			comment = Comment(blog = blog, nickname = request.POST['name'], email = request.POST['email'], content = request.POST['text'], post_date = timezone.now())
			comment.save()
			
		except (KeyError, Comment.DoesNotExist):
			return render(request, 'blog/blogHome.html', {'error_message': "you did not file out all text fields."})
		else:
			return HttpResponseRedirect(reverse('blog:blogEntry', args =(blog.id,)))

		
		
def nuke (request):
	for q in Blog.objects.all():
		q.delete()
	return HttpResponseRedirect(reverse('blog:blogHome'))
	
def init(request):
	
	nuke(request)
	num = 0
	for i in range(7):
		num += 1
		post = Blog(title = f"BLOG TITLE #{num}", author = "FIGDOG", content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer faucibus, odio et efficitur sagittis, libero ante laoreet massa, vel maximus velit leo interdum risus. Fusce dui tortor, faucibus et pretium porttitor, ullamcorper eu odio. Nullam tincidunt vel odio vitae tincidunt. Quisque congue purus ut lectus posuere ultricies. Mauris a diam lobortis, tempor nibh vel, consequat lorem. Mauris et justo ultrices, ornare est id, tincidunt diam. Sed eget mi vitae nunc efficitur cursus sed quis leo. In purus felis, consequat ac neque vitae, molestie ultricies ipsum. Proin tempus faucibus sem, vel facilisis urna scelerisque eu. Phasellus sapien diam, lacinia ut felis vel, lobortis viverra sapien. Aenean quis ullamcorper lacus, vitae suscipit orci. Aliquam bibendum quam metus, nec tristique libero convallis at. Pellentesque congue, nisl eget scelerisque interdum, metus mauris auctor metus, et maximus massa odio ut arcu. Aenean ac felis vestibulum turpis pellentesque finibus bibendum pellentesque elit. Morbi lobortis rutrum mauris eget sollicitudin. Ut vitae finibus libero. Proin sollicitudin molestie mattis. Nulla facilisis nisl sed arcu feugiat, eu efficitur erat tempus. Donec placerat risus id lorem aliquet, vel mollis dui pellentesque. Phasellus pharetra faucibus odio, at accumsan felis tristique sed. Donec id ipsum suscipit, feugiat mauris eu, dignissim elit. Nulla vel odio tortor. Integer finibus ornare malesuada. Etiam consectetur dolor eget lectus laoreet, vitae congue turpis fringilla. Donec luctus mauris vestibulum, posuere justo eu, molestie nulla. Etiam eu nunc quis odio mollis consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus.Integer a dui id metus tristique rutrum varius eu quam. In viverra mattis lacus, ac egestas tellus fringilla vel. Phasellus vel placerat turpis. Cras eget ipsum nec nulla fringilla faucibus. Nulla facilisi. In gravida nunc ac mi dignissim rhoncus. Pellentesque faucibus hendrerit dui non placerat. Ut sodales facilisis ex. Maecenas aliquet in ex non lobortis. In accumsan, diam id vulputate fermentum, neque lorem facilisis tortor, vel auctor erat justo quis augue. Nunc at dui sed purus ullamcorper feugiat in at eros. Suspendisse eu cursus mi, at egestas tellus. Fusce tincidunt accumsan egestas. Nulla luctus diam eu magna efficitur pellentesque.", post_date = timezone.now())
		post.save()
		
		for j in range(5):
			comment = Comment(blog = post, nickname = f"Figbot #{j} blog ID {num} ", email = f"Bot010010{j}@bot.com", content = "Beep boop beep beep beeeeeeeep doo doo beeeeep beep eep", post_date = timezone.now())
			comment.save()

	return HttpResponseRedirect(reverse('blog:blogHome'))
	
def make(request):

	post = Blog(title = f"BLOG TITLE MY BOT", author = "FIGDOG", content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer faucibus, odio et efficitur sagittis, libero ante laoreet massa, vel maximus velit leo interdum risus. Fusce dui tortor, faucibus et pretium porttitor, ullamcorper eu odio. Nullam tincidunt vel odio vitae tincidunt. Quisque congue purus ut lectus posuere ultricies. Mauris a diam lobortis, tempor nibh vel, consequat lorem. Mauris et justo ultrices, ornare est id, tincidunt diam. Sed eget mi vitae nunc efficitur cursus sed quis leo. In purus felis, consequat ac neque vitae, molestie ultricies ipsum. Proin tempus faucibus sem, vel facilisis urna scelerisque eu. Phasellus sapien diam, lacinia ut felis vel, lobortis viverra sapien. Aenean quis ullamcorper lacus, vitae suscipit orci. Aliquam bibendum quam metus, nec tristique libero convallis at. Pellentesque congue, nisl eget scelerisque interdum, metus mauris auctor metus, et maximus massa odio ut arcu. Aenean ac felis vestibulum turpis pellentesque finibus bibendum pellentesque elit. Morbi lobortis rutrum mauris eget sollicitudin. Ut vitae finibus libero. Proin sollicitudin molestie mattis. Nulla facilisis nisl sed arcu feugiat, eu efficitur erat tempus. Donec placerat risus id lorem aliquet, vel mollis dui pellentesque. Phasellus pharetra faucibus odio, at accumsan felis tristique sed. Donec id ipsum suscipit, feugiat mauris eu, dignissim elit. Nulla vel odio tortor. Integer finibus ornare malesuada. Etiam consectetur dolor eget lectus laoreet, vitae congue turpis fringilla. Donec luctus mauris vestibulum, posuere justo eu, molestie nulla. Etiam eu nunc quis odio mollis consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus.Integer a dui id metus tristique rutrum varius eu quam. In viverra mattis lacus, ac egestas tellus fringilla vel. Phasellus vel placerat turpis. Cras eget ipsum nec nulla fringilla faucibus. Nulla facilisi. In gravida nunc ac mi dignissim rhoncus. Pellentesque faucibus hendrerit dui non placerat. Ut sodales facilisis ex. Maecenas aliquet in ex non lobortis. In accumsan, diam id vulputate fermentum, neque lorem facilisis tortor, vel auctor erat justo quis augue. Nunc at dui sed purus ullamcorper feugiat in at eros. Suspendisse eu cursus mi, at egestas tellus. Fusce tincidunt accumsan egestas. Nulla luctus diam eu magna efficitur pellentesque.", post_date = timezone.now())
	post.save()
		
	for j in range(5):
		comment = Comment(blog = post, nickname = f"Figbot #{j} ", email = f"Bot010010{j}@bot.com", content = "Beep boop beep beep beeeeeeeep doo doo beeeeep beep eep", post_date = timezone.now())
		comment.save()
		
	return HttpResponseRedirect(reverse('blog:blogHome'))