from django.urls import path

from . import views

app_name = 'blog'

urlpatterns = [
	path('menu', views.menu, name = 'menu'),
	path('aboutMe', views.aboutMe , name = 'aboutMe'),
	path('gitNotes', views.gitNotes , name = 'gitNotes'),
	path('init' , views.init , name = 'init'),
	path('', views.blogHome, name = 'blogHome'),
	path('blogArchive', views.blogArchive, name = 'blogArchive'),
	path('<int:blog_id>/blog ', views.blogEntry, name = 'blogEntry'),
	path('nuke', views.nuke, name = 'nuke'),
	path('make', views.make, name = 'make'),
	path('comment/<int:id>', views.comment, name = 'comment'),
	
]
