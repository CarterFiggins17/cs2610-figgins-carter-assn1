import datetime

from django.db import models
from django.utils import timezone


class Blog(models.Model):
	title = models.CharField(max_length = 200)
	author = models.CharField(max_length = 200)
	content = models.CharField(max_length = 2400)
	post_date = models.DateTimeField('date posted')
	
	def __str__(self):
		return f"[{self.id}] {self.title}"
	def was_published_recently(self):
		return self.post_date >= timezone.now() - datetime.timedelta(days=1)
	

class Comment(models.Model):
	blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
	nickname = models.CharField(max_length = 200)
	email = models.CharField(max_length = 200)
	content = models.CharField(max_length = 1200)
	post_date = models.DateTimeField('date posted')
	def __str__(self):
		return f"[{self.id}] {self.nickname}"
		
		
		
		
